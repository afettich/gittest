# WERA2GRIB

## Usage:

*./wera2grib.py wuv_Triest_combined_202003290030.csv  Triest output_folder*

Input za *wera2grib.py* je datoteka **wuv_Triest_combined_202003290030.csv**.

## Settings:

V skripti options.h se nastavi :
* *RUNDATE* : datum, ki nas zanima.

## Requirements:

Potrebujemo .csv datoteko, ki jo želimo pretvoriti v .grib datoteko in include skripto: *options.h*

* **wera2grib.py** 
* **include** : options.h
